#!/bin/bash -eux

rm -rf example/migrations/*.{py,sql}

: yoyo CLI is enabled.

python -m example yoyo --help

: Create new migration

EDITOR=/bin/true python -m example yoyo --batch new --sql

: Apply migrations

python -m example yoyo --batch apply
