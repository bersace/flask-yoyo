from flask import Blueprint, Flask, jsonify

from flask_yoyo import Yoyo


example = Blueprint('example', 'example')


@example.route("/")
def index():
    return jsonify({"message": "Hello world."})


def create_app():
    app = Flask(__name__, static_folder=None)

    # Beware that app config must be loaded before you initialize extension
    # with app.
    app.config.from_pyfile('config.py')

    Yoyo(app)

    app.register_blueprint(example)

    return app
