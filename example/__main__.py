#
# Run with python -m example.
#
# Configure Postgres access with regular libpq env vars.
#

import logging
import os
import pdb
import sys

import click
from flask.cli import FlaskGroup

from .app import create_app


@click.group(cls=FlaskGroup, create_app=create_app)
def main(argv=sys.argv[1:]):
    pass


logger = logging.getLogger(__name__)


logging.basicConfig(
    level=logging.INFO,
    format=f'%(levelname)1.1s [%(name)s]  %(message)s',
)
logger.setLevel(logging.DEBUG)

try:
    exit(main())
except (pdb.bdb.BdbQuit, KeyboardInterrupt):
    logger.info("Interrupted.")
except Exception:
    logger.exception('Unhandled error:')
    if sys.stdout.isatty():
        logger.debug("Dropping in debugger.")
        pdb.post_mortem(sys.exc_info()[2])

exit(os.EX_SOFTWARE)
