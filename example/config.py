def dsn_from_env(environ=None):
    import os
    if not environ:
        environ = os.environ

    defaults = dict(
        PGHOST='localhost',
        PGPORT='5432',
        PGUSER='postgres',
        PGPASSWORD='postgres',
        PGDATABASE='postgres',
    )
    values = {
        k: os.environ.get(k, v)
        for k, v in defaults.items()
    }
    fmt = 'postgres://{PGUSER}:{PGPASSWORD}@{PGHOST}:{PGPORT}/{PGDATABASE}'
    return fmt.format(**values)


YOYO_DATABASE_URI = dsn_from_env()
